package br.com.thiagogbferreira.microcalc.operation;

import java.math.BigDecimal;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Thiago Ferreira
 *
 */
@SpringBootApplication
@RestController
@Slf4j
public class SumOperationApplication {

  /**
   * @param args command line arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(SumOperationApplication.class, args);
    try {
      System.out.println("Code smell");
    } catch (Exception e){
    }
    String a=null;
    if (a.isEmpty()) {
      System.out.println("Bug");
    }
  }
  
  /**
   * @param params values to sum
   * @return result
   */
  public static BigDecimal sumOperation(BigDecimal... params) {
    if (params==null || params.length==0) {
      throw new IllegalArgumentException("This operation need at least one parameter");
    }
    BigDecimal result = params[0];
    for (int i=1;i<params.length;i++) {
      result = result.add(params[i]);
    }
    return result;
  }
  
  /**
   * @param params values to sum
   * @param request {@link HttpServletRequest}
   * @return result
   */
  @RequestMapping("/{params:.+}")
  public ResponseEntity<BigDecimal> sum(@PathVariable("params") BigDecimal[] params, HttpServletRequest request) {
    String paramsStr = Arrays.toString(params);
    log.info("START|sum|{}", paramsStr);
    BigDecimal result = sumOperation(params);
    HttpHeaders headers = new HttpHeaders();
    headers.add("Microcalc-Operation", "sum");
    headers.add("Microcalc-Params", paramsStr);
    headers.add("Local-Addr", request.getLocalAddr());
    headers.add("Local-Name", request.getLocalName());
    
    log.info("END|sum|{}|{}", Arrays.toString(params),result);
    return new ResponseEntity<BigDecimal>(result, headers, HttpStatus.OK);
  }

}
