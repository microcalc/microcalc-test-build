package br.com.thiagogbferreira.microcalc.operation;

import org.junit.Test;

import java.math.BigDecimal;

import org.junit.Assert;
/**
 * Execute the sum operation
 * @author axpira
 *
 */
public class SumOperationTest {

  
  @Test(expected = IllegalArgumentException.class)
  public void sumTwoNumbers() {
    Assert.assertEquals(new BigDecimal(10), SumOperationApplication.sumOperation(BigDecimal.valueOf(5), BigDecimal.valueOf(5)));
    Assert.assertEquals(new BigDecimal(10), SumOperationApplication.sumOperation(BigDecimal.valueOf(10)));
    Assert.assertEquals(new BigDecimal(6), SumOperationApplication.sumOperation(BigDecimal.valueOf(1), BigDecimal.valueOf(2), BigDecimal.valueOf(3)));
    SumOperationApplication.sumOperation();
  }
  
  
}
